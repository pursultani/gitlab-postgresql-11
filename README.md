# GitLab PostgreSQL 11 Validation

This repository contains Terraform scripts for setting up PostgreSQL cluster with GitLab Omnibus package on GCE.
It was written for verifying PostgreSQL 11 HA.

### Variables

You MUST set the following values:

| Name              | Description  |
|-------------------|--------------|
| `gitlab_token`    | Your GitLab token for downloading artifacts with GitLab API |
| `gitlab_artifact` | The URL of the package to install with APT (must be `.deb`) |
| `gitlab_domain`   | The domain that is used as `external_url` |
| `ssh_user`        | Your SSH user to login to GCE instances |
| `ssh_key`         | The path to your SSH key. It must not be encrypted. |
| `prefix`          | The prefix of instance names |
| `project`         | Your GCE project name |
| `region`          | GCE Region |
| `zone`            | GCE Zone |
