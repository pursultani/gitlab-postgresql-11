#!/bin/bash

if [[ "${1}" == *-db-* &&  "${REPMGR_VERSION}" = '4' ]]; then
  sudo sed -i \
    's/SCHEMA repmgr_\S* /SCHEMA repmgr /g' \
    /opt/gitlab/embedded/cookbooks/repmgr/recipes/consul_user_permissions.rb

  sudo sed -i \
    's/repmgr\.conf/repmgr.conf --daemonize=false --no-pid-file/g' \
    /opt/gitlab/embedded/cookbooks/repmgr/templates/default/sv-repmgrd-run.erb

  sudo sed -i \
    '/^witness_repl_nodes_sync_interval_secs/d' \
    /opt/gitlab/embedded/cookbooks/repmgr/templates/default/repmgr.conf.erb
fi
