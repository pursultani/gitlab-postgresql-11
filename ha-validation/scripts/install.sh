#!/bin/bash

curl -L -H "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "${GITLAB_ARTIFACT}" -o artifact.deb
sudo dpkg -i artifact.deb
rm -f artifact.deb
