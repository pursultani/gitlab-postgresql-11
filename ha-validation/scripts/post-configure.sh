#!/bin/bash

if [[ "${1}" == *-app ]]; then
  sleep 20
  sudo gitlab-rake gitlab:db:configure
elif [[ ! "${1}" == *-db-0 ]]; then
  sleep 10
  sudo gitlab-ctl repmgr standby setup "${MASTER_IP}" -w
fi
