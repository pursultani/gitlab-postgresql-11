#!/bin/bash

CONSUL_USERNAME='gitlab-consul'
CONSUL_DATABASE_PASSWORD='CONSUL_DATABASE_PASSWORD'
CONSUL_PASSWORD_HASH="$(echo "${CONSUL_DATABASE_PASSWORD}" | gitlab-ctl pg-password-md5 ${CONSUL_USERNAME})"
CONSUL_SERVER_NODES="${DATABASE_IPS}"

POSTGRESQL_USERNAME='gitlab'
POSTGRESQL_USER_PASSWORD='POSTGRESQL_USER_PASSWORD'
POSTGRESQL_PASSWORD_HASH="$(echo "${POSTGRESQL_USER_PASSWORD}" | gitlab-ctl pg-password-md5 ${POSTGRESQL_USERNAME})"

PGBOUNCER_USERNAME='pgbouncer'
PGBOUNCER_PASSWORD='PGBOUNCER_PASSWORD'
PGBOUNCER_PASSWORD_HASH="$(echo "${PGBOUNCER_PASSWORD}" | gitlab-ctl pg-password-md5 ${PGBOUNCER_USERNAME})"
PGBOUNCER_NODE="127.0.0.1"

if [[ "${1}" == *-app ]]; then
  sudo tee /etc/gitlab/gitlab.rb <<-EOF
    external_url '${GITLAB_DOMAIN}'

    gitlab_rails['db_host'] = '${PGBOUNCER_NODE}'
    gitlab_rails['db_port'] = 6432
    gitlab_rails['db_password'] = '${POSTGRESQL_USER_PASSWORD}'
    gitlab_rails['auto_migrate'] = false

    postgresql['enable'] = false
    pgbouncer['enable'] = true
    consul['enable'] = true

    pgbouncer['admin_users'] = %w(pgbouncer gitlab-consul)
    pgbouncer['users'] = {
      'gitlab-consul': {
        password: '${CONSUL_PASSWORD_HASH}'
      },
      'pgbouncer': {
        password: '${PGBOUNCER_PASSWORD_HASH}'
      }
    }

    consul['watchers'] = %w(postgresql)
    consul['configuration'] = {
      retry_join: %w(${CONSUL_SERVER_NODES})
    }
EOF
else
  if [[ ! "${1}" == *-db-0 ]]; then
    EXTRA_CONFIG="repmgr['master_on_initialization'] = false"
  fi

  sudo tee /etc/gitlab/gitlab.rb <<-EOF
    roles ['consul_role', 'postgres_role']

    gitlab_rails['auto_migrate'] = false

    postgresql['version'] = ${POSTGRESQL_VERSION}
    postgresql['listen_address'] = '0.0.0.0'
    postgresql['hot_standby'] = 'on'
    postgresql['wal_level'] = 'replica'
    postgresql['shared_preload_libraries'] = '$(if [ "${REPMGR_VERSION}" = "4" ]; then echo 'repmgr'; else echo 'repmgr_funcs'; fi)'
    postgresql['max_wal_senders'] = 4
    postgresql['max_replication_slots'] = 4

    postgresql['pgbouncer_user_password'] = '${PGBOUNCER_PASSWORD_HASH}'
    postgresql['sql_user_password'] = '${POSTGRESQL_PASSWORD_HASH}'
    postgresql['trust_auth_cidr_addresses'] = %w(${DATABASE_CIDRS})

    repmgr['node_name'] = '${1}'
    repmgr['trust_auth_cidr_addresses'] = %w(127.0.0.1/32 ${DATABASE_CIDRS})

    consul['services'] = %w(postgresql)
    consul['configuration'] = {
      server: true,
      retry_join: %w(${CONSUL_SERVER_NODES})
    }
    ${EXTRA_CONFIG}
EOF
fi

sudo gitlab-ctl reconfigure
