#!/bin/bash

set -eux

[ -f ./secrets.sh ] && source ./secrets.sh

sshCommand() {
  printf 'ssh -o StrictHostKeyChecking=no -i %s %s@%s' "${SSH_KEY}" "${SSH_USER}" "${1}"
}

HOSTS=(
$(gcloud compute instances list --filter "name:${PREFIX}-*" --format=json  \
  | jq -r '.[].networkInterfaces[0].accessConfigs[0].natIP')
)

for HOST in "${HOSTS[@]}"; do
  ssh-keygen -R ${HOST}
done

SPLIT_PANES=()
for HOST in "${HOSTS[@]:1}"; do
  SPLIT_PANES+=( split-window $(sshCommand "${HOST}") ';' )
done

tmux new-session $(sshCommand "${HOSTS[0]}") ';' \
  ${SPLIT_PANES[@]} \
  select-layout even-vertical
