variable project {
}

variable region {
}

variable zone {
}

variable prefix {
}

variable replica_count {
	type = number
	default = 3
}

variable machine_type {
    default = "n1-standard-1"
}

variable os_family {
	default = "debian-10"
}

variable os_project {
	default = "debian-cloud"
}

variable ssh_user {
}

variable ssh_key {
}

variable gitlab_token {
}

variable gitlab_artifact {
}

variable gitlab_domain {
	default = null
}

variable postgresql_version {
	default = "11"
}

variable repmgr_version {
	default = "4"
}
